import moment from 'moment';
import Toast from 'dike/utils/Toast';

import createAction from 'dike/utils/createAction';

import { getLastTransactions, getBalance } from 'dike/store/user/user.actions';
import {
  refreshApp,
  setLastBetLoading,
  setDrawingDetailLoading,
  setDrawingHistoryLoading,
  setPlaceBetsScreenLoading,
  setPreviousDrawingLoading,
  setPlaceBetsPopupProps
} from 'dike/store/app/app.actions';

import * as types from './drawing.types';
import drawingServices from './drawing.services';

export const removeCurrentDrawing = () => ({ type: types.REMOVE_CURRENT_DRAWING });

export const getDrawingStatus = () =>
  createAction({
    type: types.GET_DRAWING_STATUS,
    process: drawingServices.getDrawingStatus,
    onAfterSuccess({ payload, dispatch, getState }) {
      const drawingState = getState().drawing;
      if (drawingState.drawingStatus.inProgress && !payload.inProgress) {
        dispatch(removeCurrentDrawing());
      }
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(refreshApp());
    }
  });

export const getLastBet = () =>
  createAction({
    type: types.GET_LAST_BET,
    process: drawingServices.getLastBet,
    onBeforeStart({ dispatch }) {
      dispatch(setLastBetLoading(true));
    },
    onAfterSuccess({ dispatch }) {
      dispatch(setLastBetLoading(false));
    },
    onError({ dispatch }) {
      dispatch(setLastBetLoading(false));
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(refreshApp());
    }
  });

export const getDrawingDetail = id =>
  createAction({
    type: types.GET_DRAWING_DETAIL,
    payload: id,
    process: drawingServices.getDrawingDetail,
    onBeforeStart({ dispatch }) {
      dispatch(setDrawingDetailLoading(true));
    },
    onAfterSuccess({ dispatch }) {
      dispatch(setDrawingDetailLoading(false));
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(getDrawingDetail(id));
    }
  });

export const setDrawingHistoryProps = (date, participated) => ({
  type: types.SET_DRAWING_HISTORY_PROPS,
  payload: { date, participated }
});

export const getDrawingHistory = ({ date, participated }) =>
  createAction({
    type: types.GET_DRAWING_HISTORY,
    payload: {
      date:
        moment(date)
          .startOf('day')
          .unix() * 1000,
      participated
    },
    process: drawingServices.getDrawingHistory,
    onBeforeStart({ dispatch }) {
      dispatch(setDrawingHistoryLoading(true));
      dispatch(setDrawingHistoryProps(date, participated));
    },
    onAfterSuccess({ dispatch }) {
      dispatch(setDrawingHistoryLoading(false));
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(getDrawingHistory({ date, participated }));
      dispatch(refreshApp());
    }
  });

export const setCurrentDrawingStats = drawingStats => (dispatch, getState) => {
  const { drawing } = getState();
  if (!drawing.currentDrawing || drawing.currentDrawing.id !== drawingStats.drawingId) {
    return;
  }
  return dispatch({
    type: types.SET_CURRENT_DRAWING_STATS,
    payload: {
      betCount: drawingStats.betCount,
      globalStake: drawingStats.globalStake,
      stakeLeaderboard: drawingStats.stakeLeaderboard
    }
  });
};

export const setUserBets = bets => ({ type: types.SET_USER_BETS, payload: bets });

export const placeBets = (bets, onSuccess) =>
  createAction({
    type: types.PLACE_BETS,
    payload: bets,
    process: drawingServices.placeBets,
    onBeforeStart({ dispatch }) {
      dispatch(setPlaceBetsScreenLoading(true));
    },
    onError({ error, dispatch }) {
      dispatch(
        setPlaceBetsPopupProps({
          isVisible: true,
          title: 'Error',
          notice: error.graphQLErrors[0].message
        })
      );
      dispatch(setPlaceBetsScreenLoading(false));
    },
    onAfterSuccess({ payload, params, dispatch }) {
      dispatch(setPlaceBetsScreenLoading(false));
      if (payload) {
        onSuccess();
        const flatBets = [];
        params.forEach(betLot => {
          betLot.numbers.forEach(number => {
            flatBets.push({
              number,
              special: betLot.special,
              stake: betLot.stake
            });
          });
        });
        dispatch(setUserBets(flatBets));
        dispatch(getLastTransactions());
        dispatch(getBalance('dike'));
        Toast.show('Bet placed!!!');
      } else {
        dispatch(
          setPlaceBetsPopupProps({
            isVisible: true,
            title: 'Error',
            notice: 'Bets can not be placed!'
          })
        );
      }
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(placeBets(bets));
      dispatch(refreshApp());
    }
  });

export const getPreviousDrawing = () =>
  createAction({
    type: types.GET_PREVIOUS_DRAWING,
    process: drawingServices.getPreviousDrawing,
    onBeforeStart({ dispatch }) {
      dispatch(setPreviousDrawingLoading(true));
    },
    onAfterSuccess({ dispatch }) {
      dispatch(setPreviousDrawingLoading(false));
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(refreshApp());
    }
  });

export const getDrawingChart = () =>
  createAction({ type: types.GET_DRAWING_CHART, process: drawingServices.getDrawingChart });