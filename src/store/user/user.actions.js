import Toast from 'dike/utils/Toast';

import createAction from 'dike/utils/createAction';
import { setToken } from 'dike/utils/graphClients';
import { setStorageItem, getStorageItem, removeStorageItem } from 'dike/utils/asyncStorage';

import {
  refreshApp,
  setLoginScreenLoading,
  setRegisterScreenLoading,
  setTransactionsScreenLoading,
  setVerifyEmailScreenLoading,
  setWalletScreenLoading,
  setWalletPopupProps,
  setForgotPasswordScreenProps,
  setChangePasswordScreenProps,
  setReportIssueScreenLoading,
  setPeopleScreenLoading
} from 'dike/store/app/app.actions';

import userServices from './user.services';
import * as types from './user.types';

export const me = () =>
  createAction({
    type: types.ME,
    process: userServices.me,
    onAfterSuccess: async ({ payload, dispatch, getState, socketMiddleware }) => {
      const storeState = getState();
      if (storeState.user._id) {
        socketMiddleware.addListeners(dispatch, getState);
      } else {
        await removeStorageItem('userToken');
      }

      if (storeState.app.isLoginScreenLoading) {
        dispatch(setLoginScreenLoading(false));
      }
      if (storeState.app.isRegisterScreenLoading) {
        dispatch(setRegisterScreenLoading(false));
      }
    },
    onError: async ({ dispatch }) => {
      dispatch(setLoginScreenLoading(false));
      dispatch(setRegisterScreenLoading(false));
      await removeStorageItem('userToken');
    }
  });

export const login = ({ email, password }) =>
  createAction({
    type: types.LOGIN,
    payload: { email, password },
    process: userServices.login,
    onBeforeStart({ dispatch }) {
      dispatch(setLoginScreenLoading(true));
    },
    onAfterSuccess: async ({ payload, dispatch }) => {
      try {
        setToken(payload.token);
        console.log('vl Token', payload.token);
        await setStorageItem('userToken', payload.token);
        dispatch(checkToken());
      } catch (e) {
        return;
      }
    },
    onError({ dispatch }) {
      dispatch(setLoginScreenLoading(false));
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(login({ email, password }));
    }
  });

export const checkToken = () => async (dispatch, getState) => {
  const appState = getState().app;
  if (!appState.isLoginScreenLoading) {
    dispatch(setLoginScreenLoading(true));
  }

  try {
    let userToken;
    userToken = await getStorageItem('userToken');
    if (userToken) {
      setToken(userToken);
      dispatch(me());
    } else {
      dispatch(setLoginScreenLoading(false));
      dispatch(setRegisterScreenLoading(false));
    }
  } catch (e) {
    return;
  }
};

export const register = ({ username, password, email, refererCode }) =>
  createAction({
    type: types.REGISTER,
    payload: { username, password, email, refererCode },
    process: userServices.register,
    onBeforeStart: ({ dispatch }) => {
      dispatch(setRegisterScreenLoading(true));
    },
    onAfterSuccess: async ({ payload, dispatch }) => {
      try {
        setToken(payload.token);
        // BUG: with hot reload
        await setStorageItem('userToken', payload.token);
        await dispatch(checkToken());
      } catch (e) {
        //console.log(e);
        return;
      }
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(register({ username, password, email, refererCode }));
    }
  });

export const clearRegisterError = () => ({ type: types.CLEAR_REGISTER_ERROR });
export const clearLoginError = () => ({ type: types.CLEAR_LOGIN_ERROR });

export const changeTransactionHistoryDate = ({ fromDate, toDate }) => ({
  type: types.CHANGE_TRANSACTION_HISTORY_DATE,
  payload: { fromDate, toDate }
});

export const getTransactionHistory = ({ fromDate, toDate }) =>
  createAction({
    type: types.GET_TRANSACTION_HISTORY,
    process: userServices.getTransactionHistory,
    payload: { fromDate, toDate },
    onBeforeStart({ dispatch }) {
      dispatch(setTransactionsScreenLoading(true));
    },
    onAfterSuccess({ dispatch }) {
      dispatch(setTransactionsScreenLoading(false));
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(getTransactionHistory({ fromDate, toDate }));
    }
  });

export const resetDemoBalance = () =>
  createAction({
    type: types.RESET_DEMO_BALANCE,
    process: userServices.resetDemoBalance,
    onBeforeStart({ dispatch }) {
      dispatch(setWalletScreenLoading(true));
    },
    onAfterSuccess({ payload, dispatch }) {
      if (payload) {
        dispatch(getBalance('dike'));
        dispatch(getLastTransactions());
        dispatch(
          setWalletPopupProps({
            isVisible: true,
            title: 'Dike',
            notice: 'Your balance has been reset!'
          })
        );
      } else {
        dispatch(
          setWalletPopupProps({
            isVisible: true,
            title: 'Dike',
            notice: 'Not sufficient to reset balance'
          })
        );
      }
      dispatch(setWalletScreenLoading(false));
    },
    onError({ error, dispatch }) {
      dispatch(setWalletScreenLoading(false));
      try {
        dispatch(
          setWalletPopupProps({
            isVisible: true,
            title: 'Error',
            notice: error.graphQLErrors[0].message
          })
        );
      } catch (e) {
        return;
      }
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(resetDemoBalance());
    }
  });

export const emailVerified = () => ({ type: types.EMAIL_VERIFIED });
export const setVerifyEmailError = value => ({
  type: types.SET_VERIFY_EMAIL_ERROR,
  payload: value
});
export const verifyEmail = code =>
  createAction({
    type: types.VERIFY_EMAIL,
    process: userServices.verifyEmail,
    payload: code,
    onBeforeStart({ dispatch }) {
      dispatch(setVerifyEmailScreenLoading(true));
    },
    onAfterSuccess({ payload, dispatch }) {
      if (payload) {
        dispatch(emailVerified());
        return;
      }
      dispatch(setVerifyEmailError(true));
      dispatch(setVerifyEmailScreenLoading(false));
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(verifyEmail(code));
    }
  });

export const resendVerifyCode = () =>
  createAction({
    type: types.RESEND_VERIFY_CODE,
    process: userServices.resendVerifyCode,
    onBeforeStart({ dispatch }) {
      dispatch(setVerifyEmailScreenLoading(true));
    },
    onAfterSuccess({ dispatch, payload }) {
      dispatch(setVerifyEmailScreenLoading(false));
      if (payload) {
        Toast.show('We have sent you a new verification code');
      } else {
        Toast.show('You must wait 15 seconds before requesting a new verification code', {
          type: 'danger'
        });
      }
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(resendVerifyCode());
    }
  });

export const getLastTransactions = () =>
  createAction({
    type: types.GET_LAST_TRANSACTIONS,
    process: userServices.getLastTransactions,
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(refreshApp());
    }
  });

export const getBalance = asset =>
  createAction({ type: types.GET_BALANCE, process: userServices.getBalance, payload: { asset } });

export const signOut = () => async (dispatch, getState, { socketMiddleware }) => {
  socketMiddleware.disconnect();
  setToken('');
  await removeStorageItem('userToken');
  return dispatch({ type: types.SIGN_OUT });
};

export const sendForgotPasswordEmail = (email, onSuccess, options = {}) =>
  createAction({
    type: types.SEND_FORGOT_PASSWORD_EMAIL,
    process: userServices.sendForgotPasswordEmail,
    payload: email,
    onBeforeStart({ dispatch }) {
      dispatch(setForgotPasswordScreenProps({ isLoading: true, error: '' }));
    },
    onAfterSuccess({ dispatch, payload }) {
      dispatch(setForgotPasswordScreenProps({ isLoading: false }));
      if (payload) {
        !options.noToast && Toast.show('We have sent you an email');
        onSuccess && onSuccess();
      } else {
        !options.noToast &&
          Toast.show('You must wait 15 seconds before requesting a new verification code', {
            type: 'error'
          });
      }
    },
    onError({ dispatch, error }) {
      dispatch(
        setForgotPasswordScreenProps({ error: error.graphQLErrors[0].message, isLoading: false })
      );
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(sendForgotPasswordEmail(email, onSuccess, options));
    }
  });

export const setPassword = ({ email, password, code }, onSuccess) =>
  createAction({
    type: types.SET_PASSWORD,
    process: userServices.setPassword,
    payload: { email, password, code },
    onBeforeStart({ dispatch }) {
      dispatch(setForgotPasswordScreenProps({ isLoading: true, errorSetPassword: '' }));
    },
    onAfterSuccess({ dispatch, payload }) {
      dispatch(setForgotPasswordScreenProps({ isLoading: false }));
      if (payload) {
        onSuccess && onSuccess();
      }
    },
    onError({ error, dispatch }) {
      dispatch(
        setForgotPasswordScreenProps({
          isLoading: false,
          errorSetPassword: error.graphQLErrors[0].message
        })
      );
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(setPassword({ email, password, code }, onSuccess));
    }
  });

export const changePassword = ({ oldPassword, newPassword }, onSuccess) =>
  createAction({
    type: types.CHANGE_PASSWORD,
    process: userServices.changePassword,
    payload: { oldPassword, newPassword },
    onBeforeStart({ dispatch }) {
      dispatch(setChangePasswordScreenProps({ isLoading: true, errorSubmit: '' }));
    },
    onAfterSuccess({ dispatch, payload }) {
      dispatch(setChangePasswordScreenProps({ isLoading: false }));
      if (payload) {
        onSuccess && onSuccess();
      }
    },
    onError({ error, dispatch }) {
      dispatch(
        setChangePasswordScreenProps({
          isLoading: false,
          errorSubmit: error.graphQLErrors[0].message
        })
      );
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(changePassword({ oldPassword, newPassword }, onSuccess));
    }
  });

export const reportIssue = ({ title, description }, onSuccess) =>
  createAction({
    type: types.REPORT_ISSUE,
    process: userServices.reportIssue,
    payload: { title, description },
    onBeforeStart({ dispatch }) {
      dispatch(setReportIssueScreenLoading(true));
    },
    onAfterSuccess({ dispatch, payload }) {
      dispatch(setReportIssueScreenLoading(false));
      if (payload) {
        onSuccess && onSuccess();
      }
    },
    onError({ error, dispatch }) {
      dispatch(setReportIssueScreenLoading(false));
    },
    onNetworkFailedConfirm({ dispatch }) {
      dispatch(reportIssue({ title, description }, onSuccess));
    }
  });

export const getRanking = () =>
  createAction({ type: types.GET_RANKING, process: userServices.getRanking });

export const editUserProfile = ({ displayName, profilePicture }, onSuccess) =>
  createAction({
    type: 'user/EDIT_PROFILE',
    process: userServices.editProfile,
    payload: { displayName, profilePicture },
    onAfterSuccess({ dispatch, payload }) {
      dispatch(me());
      if (payload) {
        onSuccess && onSuccess();
      }
    }
  });

export const fetchUserData = (userId, callback) =>
  createAction({
    type: 'user/FETCH_USER_DATA',
    process: userServices.fetchUserData,
    payload: { userId },
    onBeforeStart({ dispatch }) {
      dispatch(setPeopleScreenLoading(true));
    },
    onAfterSuccess({ dispatch, payload }) {
      dispatch(setPeopleScreenLoading(false));
      callback(payload);
    },
    onError({ dispatch }) {
      dispatch(setPeopleScreenLoading(true));
    }
  });

export const fetchAllNearby = (userIds, callback) =>
  createAction({
    type: 'user/FETCH_ALL_NEARBY',
    process: userServices.fetchAllNearby,
    payload: { userIds },
    onAfterSuccess({ dispatch, payload }) {
      callback(payload);
    },
    onError({ dispatch }) {
      callback(null);
    }
  });

export const updatePrivacyRule = (allowMember, allowService) =>
  createAction({
    type: 'user/UPDATE_PRIVACY_RULE',
    process: userServices.updatePrivacyRule,
    payload: { allowMember, allowService },
    onAfterSuccess({ dispatch }) {}
  });

export const sendPoints = (address, amount, privateKey) =>
  createAction({
    type: 'user/SEND_POINTS',
    process: userServices.sendPoints,
    payload: { address, amount, privateKey },
    onAfterSuccess({ dispatch, payload }) {
      Toast.show(`tx#${payload}`);
    }
  });
