import * as Types from './nearby.types';

import createAction from 'dike/utils/createAction';
import nearbyServices from './nearby.services';
import * as NearbyManager from '../../../NearbyManager';

export const startAdvertising = () => (dispatch, getState) => {
  NearbyManager.startAdvertisingWithUsername(getState().user._id);
  dispatch({
    type: Types.START_ADVERTISING
  });
};

export const stopAdvertising = () => {
  NearbyManager.stopAdvertising();
  return {
    type: Types.STOP_ADVERTISING
  };
};

export const fetchNearby = users => {
  return createAction({
    type: Types.FOUND_USERS,
    process: nearbyServices.fetchAllNearby,
    payload: { users },
    onAfterSuccess: async ({ dispatch, payload }) => {
      dispatch({
        type: Types.FOUND_USERS,
        payload
      });
    }
  });
};
