export const START_ADVERTISING = 'nearby/START_ADVERTISING';
export const STOP_ADVERTISING = 'nearby/STOP_ADVERTISING';
export const FOUND_USERS = 'nearby/FOUND_USERS';
