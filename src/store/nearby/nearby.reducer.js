import * as Types from './nearby.types';

export default (
  state = {
    advertising: false,
    nearbyUsers: []
  },
  action
) => {
  switch (action.type) {
    case Types.START_ADVERTISING:
      return {
        ...state,
        advertising: true
      };
    case Types.STOP_ADVERTISING:
      return {
        ...state,
        advertising: false
      };
    case Types.FOUND_USERS:
      return {
        ...state,
        nearbyUsers: [...action.payload]
      };
  }
  return state;
};
