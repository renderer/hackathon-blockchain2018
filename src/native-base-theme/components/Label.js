import dikeLabelTheme from 'dike/theme/components/Label';
import variable from './../variables/platform';

export default (variables = variable) => {
  const labelTheme = {
    '.focused': {
      width: 0
    },
    fontSize: 17,
    ...dikeLabelTheme(variables)
  };

  return labelTheme;
};
