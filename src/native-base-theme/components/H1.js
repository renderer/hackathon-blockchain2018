import dikeThemeH1 from 'dike/theme/components/H1';
import variable from './../variables/platform';

export default (variables = variable) => {
  const h1Theme = {
    color: variables.textColor,
    fontSize: variables.fontSizeH1,
    lineHeight: variables.lineHeightH1,
    ...dikeThemeH1(variables)
  };

  return h1Theme;
};
