import { connect } from 'react-redux';
import RegisterScreen from './RegisterScreen.component';

import { register, clearRegisterError } from 'dike/store/user/user.actions';

export default connect(
  ({ user, app }) => ({
    registerError: user.registerError,
    isLoading: app.isRegisterScreenLoading
  }),
  dispatch => ({
    onRegister(user) {
      dispatch(register(user));
    },
    onClearError() {
      dispatch(clearRegisterError());
    }
  })
)(RegisterScreen);
