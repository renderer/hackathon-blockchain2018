import { connect } from 'react-redux';
import LoginScreen from './LoginScreen.component';

import { login, clearLoginError } from 'dike/store/user/user.actions';

export default connect(
  ({ app, user }) => ({
    isLoading: app.isLoginScreenLoading,
    isActive: user.activated,
    loginError: user.loginError
  }),
  dispatch => ({
    onLogin(user) {
      dispatch(login(user));
    },
    onClearError() {
      dispatch(clearLoginError());
    }
  })
)(LoginScreen);
