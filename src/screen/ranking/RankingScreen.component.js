import React, { Component } from 'react';
import BigNumber from 'bignumber.js';

import { isEven } from 'dike/utils/common/numbers';
import { View, Content, Text, H3, Icon, H1, Card, CardItem } from 'native-base';
import DikeBackground from 'dike/components/DikeBackground';
import ScreenLoading from 'dike/components/ScreenLoading';

// const defaultData = [];
// for (let i = 0; i <= 20; i++) {
//   defaultData.push({
//     username: `John Doe ${i}`,
//     point: 70000 - 100 * i
//   });
// }

const getRankingColor = i => {
  switch (i) {
    case 0:
      return 'gold';
    case 1:
      return 'silver';
    case 2:
      return '#965A38';
    default:
      return 'fff';
  }
};

export default ({ data, onGetRanking }) => {
  if (!data) {
    onGetRanking();
    return (
      <DikeBackground>
        <ScreenLoading />
      </DikeBackground>
    );
  }
  const bestPlayer = data[0];
  const runnersUp = data.slice(1, 11);
  const challengers = data.slice(11, data.length);
  return (
    <DikeBackground>
      <Content>
        {bestPlayer && (
          <View center>
            <View center style={{ width: '60%' }} padVertical>
              <H3 bold center>
                Number 1 ranked player will win $10,000
              </H3>
              <Text small italic>
                The current best player is
              </Text>
              <Icon name="ios-trophy" style={{ color: 'gold', fontSize: 80 }} />
              <H3 bold>{bestPlayer.username}</H3>
              <H1 bold>Đ {new BigNumber(bestPlayer.point).toFormat(0)}</H1>
            </View>
          </View>
        )}

        {runnersUp &&
          runnersUp.length > 0 && (
            <Card bgMain>
              <CardItem header center>
                <H3 bold>Current top 10 runners up</H3>
              </CardItem>
              <CardItem fullWidth>
                <View fullWidth>
                  {runnersUp.map((row, i) => (
                    <View bgMain={isEven(i)} bgSemiDark={!isEven(i)} key={i}>
                      <View textBetween paddingRow>
                        <View horizontal center>
                          {i <= 2 && (
                            <Icon name="ios-ribbon" style={{ color: getRankingColor(i) }} />
                          )}
                          <H3 style={{ paddingLeft: i > 2 ? 22 : 0 }}> {row.username}</H3>
                        </View>
                        <View>
                          <H3>{`${i === 0 ? 'Đ ' : ' '}${new BigNumber(row.point).toFormat(0)}`}</H3>
                        </View>
                      </View>
                    </View>
                  ))}
                </View>
              </CardItem>
            </Card>
          )}

        {challengers &&
          challengers.length > 0 && (
            <Card transparent>
              <CardItem fullWidth>
                <View fullWidth>
                  {challengers.map((row, i) => (
                    <View textBetween paddingRow key={i}>
                      <View horizontal center>
                        <H3 blur style={{ paddingLeft: 22 }}>
                          {' '}
                          {row.username}
                        </H3>
                      </View>
                      <View>
                        <H3 blur>{`${i === 0 ? 'Đ ' : ' '}${new BigNumber(row.point).toFormat(0)}`}</H3>
                      </View>
                    </View>
                  ))}
                </View>
              </CardItem>
            </Card>
          )}
      </Content>
    </DikeBackground>
  );
};
