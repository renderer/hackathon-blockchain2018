import React, { Component } from 'react';
import { List, ListItem, Left, Body, Right, Text, Thumbnail, View, Button } from 'native-base';

export default class PeopleListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      discovering: false
    };
  }

  setAdvertising(advertising) {
    const { startAdvertising, stopAdvertising } = this.props;
    if (advertising) {
      startAdvertising();
    } else {
      stopAdvertising();
    }
  }

  handleClickPeople(id) {
    const { navigation } = this.props;
    navigation.navigate('PeopleScreen', { id });
  }

  render() {
    let { nearbyList, advertising } = this.props;
    return (
      <View fullWidth>
        <View fullWidth center>
          <Button
            block
            primary
            onPress={() => this.setAdvertising(!advertising)}
            style={{
              alignSelf: 'center'
            }}>
            <Text style={{ margin: 5 }}>{advertising ? 'Turn off AURA' : 'Turn on AURA'}</Text>
          </Button>
        </View>
        <List style={{ marginTop: 20 }}>
          {nearbyList &&
            nearbyList.map(people => (
              <ListItem
                avatar
                key={people.displayName}
                onPress={() => this.handleClickPeople(people._id)}>
                <Left>
                  {!people.profilePicture && (
                    <Thumbnail source={require('dike/asset/default_avatar.png')} />
                  )}
                  {people.profilePicture && <Thumbnail source={{ uri: people.profilePicture }} />}
                </Left>
                <Body>
                  <Text>{people.displayName}</Text>
                  <Text note>🥇 🏆 🏅</Text>
                </Body>
                <Right>
                  <Text note>{people.proximity}</Text>
                </Right>
              </ListItem>
            ))}
        </List>
      </View>
    );
  }
}
