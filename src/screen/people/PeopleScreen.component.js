import React, { Component } from 'react';
import {
  Text,
  View,
  Thumbnail,
  H2,
  Content,
  Button,
  Container,
  Body,
  Input,
  Form,
  Item,
  H3
} from 'native-base';
import { TouchableWithoutFeedback, Keyboard, Dimensions } from 'react-native';

import DikeBackground from 'dike/components/DikeBackground';
import ScreenLoading from 'dike/components/ScreenLoading';

const { width, height } = Dimensions.get('window');

export default class PeopleScreen extends Component {
  state = {
    people: {}
  };

  componentDidMount() {
    const id = this.props.navigation.state.params.id;
    this.props.fetchUserData &&
      this.props.fetchUserData(id, data => {
        this.setState({ people: data });
      });
  }

  render() {
    const { people, amount } = this.state;
    const { onSendPoints, isLoading, privateKey } = this.props;

    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <DikeBackground style={{ width, height }}>
          {isLoading ? (
            <ScreenLoading />
          ) : (
            <View>
              <View center paddingContent>
                {people.profilePicture && (
                  <Thumbnail large source={{ uri: people.profilePicture }} />
                )}
                <H2>{people && people.displayName}</H2>
                <Text
                  style={{
                    fontSize: 12,
                    marginTop: 5
                  }}>
                  {people && people.ethDepositAddress}
                </Text>
                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                  {people.badges
                    ? people.badges.map((badge, i) => <Text key={i}>{badge}</Text>)
                    : ''}
                </View>
              </View>

              <View
                authScreenWrapper
                style={{
                  alignSelf: 'center'
                }}>
                <Form full>
                  <Item>
                    <Input
                      keyboardType="numeric"
                      value={amount}
                      placeholder="Enter amount"
                      onChangeText={text => this.setState({ amount: text })}
                    />
                  </Item>
                  <Button
                    onPress={() => {
                      onSendPoints && onSendPoints(people.ethDepositAddress, amount, privateKey);
                      Keyboard.dismiss();
                    }}
                    block
                    placeholder
                    style={{ margin: 10 }}>
                    <Text>Send Points</Text>
                  </Button>
                </Form>
              </View>
            </View>
          )}
        </DikeBackground>
      </TouchableWithoutFeedback>
    );
  }
}
