import { connect } from 'react-redux';
import LastTransactionsCard from './LastTransactionsCard.component';

export default connect(({ user }) => ({
  transactions: user.transactions
}))(LastTransactionsCard);
