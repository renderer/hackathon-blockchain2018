import React, { Component } from 'react';
import BigNumber from 'bignumber.js';

import { TouchableOpacity, StyleSheet } from 'react-native';
import { View, Text, Card, CardItem, Left, Right, H3 } from 'native-base';

import CardLoading from 'dike/components/CardLoading';
import TimeBox from 'dike/components/TimeBox';
import DikeNumber from 'dike/components/DikeNumber';

import withDimensions from 'dike/utils/hoc/withDimensions';

const styles = StyleSheet.create({
  winningItem: { width: `${100 / 6}%` },
  footer: { justifyContent: 'space-between' }
});

const WinningBets = withDimensions()(({ bets, dimensions }) => {
  const winningBets = bets.filter(bet => bet.reward !== '0');
  if (winningBets.length < 1) {
    return <Text>You didn't win any number</Text>;
  }
  return (
    <View horizontal wrap wrapContainer>
      {winningBets.map((bet, i) => (
        <View center style={styles.winningItem} wrapItem key={i}>
          <DikeNumber
            number={bet.number}
            size={Math.floor((dimensions.width - 50) / 6)}
            special={!!bet.special}
            occurence={bet.occurence}
          />
          <Text bold>{bet.reward ? new BigNumber(`${bet.reward}`).toFormat(0) : 0}</Text>
        </View>
      ))}
    </View>
  );
});

export default class LastBetCard extends Component {
  componentDidMount() {
    this.props.onGetLastBet();
  }
  render() {
    const { isLoading, bets, drawingDate, onNavigate, drawingId, totalReward } = this.props;

    if (!!isLoading) {
      return <CardLoading />;
    }

    const participated = bets && bets.length > 0;
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        onPress={() => onNavigate && participated && onNavigate(drawingId)}>
        <Card bgMain>
          <CardItem header borderBottom>
            <Left>
              <H3 bold>Most recent bets</H3>
            </Left>
            {!!participated && (
              <Right>
                <TimeBox time={drawingDate} />
              </Right>
            )}
          </CardItem>
          <CardItem>
            {!participated && <Text>You have not participated in any drawing yet.</Text>}
            {participated && <WinningBets bets={bets} key="winBets" />}
          </CardItem>
          {participated && (
            <CardItem footer style={styles.footer}>
              <View>
                <Text highlight>View Detail</Text>
              </View>
              <H3 important bold>
                Total reward: {new BigNumber(totalReward).toFormat(0)}
              </H3>
            </CardItem>
          )}
        </Card>
      </TouchableOpacity>
    );
  }
}
