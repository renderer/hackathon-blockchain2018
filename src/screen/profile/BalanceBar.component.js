import React from 'react';
import BigNumber from 'bignumber.js';

import { TouchableOpacity } from 'react-native';
import { View, Text, H1, Icon } from 'native-base';

export default ({ balance, onResetPress, onSettingPress, activated }) => (
  <View textBetween marginAround style={{ alignItems: 'flex-end' }}>
    <TouchableOpacity onPressIn={() => onResetPress()}>
      <H1 bold larger>
        Đ {new BigNumber(balance).toFormat(0)}
      </H1>
      <Text highlight>{!!activated ? 'Reset Demo account' : 'Active account'}</Text>
    </TouchableOpacity>
    <TouchableOpacity onPressIn={() => onSettingPress()}>
      <Icon highlight larger name="ios-settings" />
    </TouchableOpacity>
  </View>
);
