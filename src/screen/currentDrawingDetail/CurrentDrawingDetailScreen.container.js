import CurrentDrawingDetailScreen from './CurrentDrawingDetailScreen.component';
import { connect } from 'react-redux';

import { getDrawingDetail } from 'dike/store/drawing/drawing.actions';

export default connect(
  ({ drawing, app }) => ({
    drawing: drawing.drawingDetail,
    isLoading: app.isDrawingDetailLoading
  }),
  dispatch => ({
    onGetDrawing(id) {
      dispatch(getDrawingDetail(id));
    }
  })
)(CurrentDrawingDetailScreen);
