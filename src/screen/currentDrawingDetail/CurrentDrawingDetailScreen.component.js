import React, { Component } from 'react';
import { tap } from 'dike/utils/sounds';

import { Content, Text, Card, CardItem, H3 } from 'native-base';

import DikeBackground from 'dike/components/DikeBackground';
import ScreenLoading from 'dike/components/ScreenLoading';
import CurrentDrawingCard from 'dike/screen/game/CurrentDrawingCard.component';

export default class CurrentDrawingDetailScreen extends Component {
  componentDidMount() {
    const { navigation, onGetDrawing } = this.props;
    const drawingId = navigation.getParam('drawingId');
    onGetDrawing(drawingId);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { navigation, drawing } = this.props;
    if (!drawing.done && nextProps.drawing.done) {
      navigation.replace('DrawingDetail', { drawingId: drawing.id });
    }
  }

  render() {
    const { isLoading, drawing, navigation } = this.props;
    return (
      <DikeBackground>
        {isLoading && <ScreenLoading />}

        {!isLoading && (
          <Content>
            <Card bgMain first>
              <CardItem header>
                <H3 bold>Drawing in progress</H3>
              </CardItem>
              <CardItem>
                <Text>The result is not yet to come. Please, check again later!</Text>
              </CardItem>
            </Card>

            <CurrentDrawingCard
              onNavigate={() => {
                navigation.navigate('PlaceBets');
                tap.play();
              }}
              drawing={drawing}
            />
          </Content>
        )}
      </DikeBackground>
    );
  }
}
