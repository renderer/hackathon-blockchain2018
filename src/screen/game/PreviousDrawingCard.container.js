import { connect } from 'react-redux';
import PreviousDrawingCard from './PreviousDrawingCard.component';

import { getPreviousDrawing } from 'dike/store/drawing/drawing.actions';

export default connect(
  ({ drawing, app }) => ({
    drawing: drawing.previousDrawing,
    isLoading: app.isPreviousDrawingLoading
  }),
  dispatch => ({
    onGetDrawing() {
      dispatch(getPreviousDrawing());
    }
  })
)(PreviousDrawingCard);
