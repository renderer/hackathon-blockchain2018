import theme from 'dike/theme';
import React from 'react';
import BigNumber from 'bignumber.js';

import { Dimensions, StyleSheet } from 'react-native';
import { View, Text, Button, Card, CardItem, Left, Right, H1, H3, Body } from 'native-base';

import Leaderboard from 'dike/components/Leaderboard';
import DikeNumber from 'dike/components/DikeNumber';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  betItem: { width: `${100 / 5}%` }
});

const getCurrentBets = drawing => {
  const normal = [];
  const special = [];
  let total = 0;
  if (drawing.userBets && drawing.userBets.length) {
    drawing.userBets.forEach(bet => {
      total += parseInt(bet.stake, 10);
      bet.special ? special.push(bet) : normal.push(bet);
    });
  }
  return { normal, special, total };
};

const BetLine = ({ bets, special = false, dimensions }) => (
  <View horizontal wrap>
    {bets.map((bet, i) => (
      <View wrapItem style={styles.betItem} key={i}>
        <DikeNumber
          size={Math.floor((width - theme.spacingUnit * 16) / 5)}
          number={bet.number}
          special={special}
        />
      </View>
    ))}
  </View>
);

export default ({ drawing, onNavigate }) => {
  const bets = getCurrentBets(drawing);
  const totalBets = new BigNumber(bets.total);
  return (
    <Card bgMain first>
      <CardItem header borderBottom>
        <Left>
          <H3 bold>Your bets</H3>
        </Left>
        <Right>
          <H3>Total stake: {totalBets.toFormat(0)}</H3>
        </Right>
      </CardItem>
      <CardItem vertical borderBottom>
        {totalBets.eq(0) && (
          <Body>
            <Text>You haven't placed any bets in this drawing!</Text>
            <Text>Win Dike every drawing, don't miss your chance, good luck!</Text>
          </Body>
        )}
        {totalBets.gt(0) && (
          <Body>
            {totalBets.gt(0) &&
              bets.special.length > 0 && <BetLine bets={bets.special} special={true} />}
            {totalBets.gt(0) &&
              bets.normal.length > 0 && <BetLine bets={bets.normal} special={false} />}
          </Body>
        )}
      </CardItem>
      {drawing &&
        drawing.drawingStats.stakeLeaderboard &&
        drawing.drawingStats.stakeLeaderboard.length > 0 && (
          <CardItem fullWidth>
            <Leaderboard
              type="stake"
              leaderboard={drawing.drawingStats.stakeLeaderboard.map(row => ({
                username: row.username,
                points: row.stake
              }))}
            />
          </CardItem>
        )}
      <CardItem center vertical>
        <H1 bold>Global stake: {new BigNumber(drawing.drawingStats.globalStake).toFormat(0)}</H1>
        <H3>Bet count: {drawing.drawingStats.betCount}</H3>
      </CardItem>
      <CardItem center>
        <View half>
          <Button large block onPress={() => onNavigate && onNavigate()}>
            <Text>Place bets</Text>
          </Button>
        </View>
      </CardItem>
    </Card>
  );
};
