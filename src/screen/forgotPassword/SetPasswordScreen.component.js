import { tap } from 'dike/utils/sounds';
import React, { Component } from 'react';

import { TouchableWithoutFeedback, Keyboard } from 'react-native';
import { View, H1, Text, Button, Form, Item } from 'native-base';
import DikeBackground from 'dike/components/DikeBackground';
import ItemInputPassword from 'dike/components/ItemInputPassword';
import ScreenOverlayLoading from 'dike/components/ScreenOverlayLoading';

import { validatePassword } from 'dike/utils/common/validators';
import { getStorageItem } from 'dike/utils/asyncStorage';

export default class SetPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      code: '',
      error: {},
      isSuccess: false
    };
  }

  async handleSubmit() {
    const { password, code } = this.state;
    const isValidPassword = validatePassword(password);
    const isValidCode = !!code;
    if (!isValidPassword || !isValidCode) {
      this.setState({ error: { password: !isValidPassword, code: !isValidCode } });
      return;
    }

    this.setState({ error: { password: false, code: false } });
    const userEmail = await getStorageItem('userEmail');
    this.props.onSubmit({ email: userEmail, password, code }, () => {
      this.setState({ isSuccess: true });
    });
  }

  render() {
    const { errorSetPassword, navigation, isLoading } = this.props;
    const { password, error, code, isSuccess } = this.state;
    return (
      <DikeBackground>
        <View full center>
          {!isSuccess && (
            <View authScreenWrapper>
              <H1 bold>Set password</H1>
              <Text endDescription>
                A verification code has been sent to your email. Enter it here to set your new
                password.
              </Text>
              {!!errorSetPassword && (
                <Text error small>
                  *{errorSetPassword}
                </Text>
              )}
              <Form full>
                <Item noLabel inputLine error={error.code}>
                  <TouchableWithoutFeedback
                    onPress={() => {
                      Keyboard.dismiss();
                      navigation.navigate('SetPasswordVerifyCode', {
                        onGoBack: code => this.setState({ code, error: { ...error, code: false } })
                      });
                    }}>
                    <View fullWidth>
                      {!code && <Text inputPlaceholder>Verification code</Text>}
                      {!!code && <Text inputValue>{code}</Text>}
                    </View>
                  </TouchableWithoutFeedback>
                </Item>
                {!!error.code && (
                  <Text error small>
                    *A verification code is required
                  </Text>
                )}

                <ItemInputPassword
                  value={password}
                  error={error.password}
                  onChangeText={v => {
                    const nextState = { password: v };
                    if (error.password) {
                      error.password = !validatePassword(v);
                      nextState.error = error;
                    }
                    this.setState(nextState);
                  }}
                />
              </Form>

              <View fullWidth center>
                <View half fullFormFooter>
                  <Button
                    block
                    primary
                    onPress={() => {
                      this.handleSubmit();
                      tap.play();
                    }}>
                    <Text>Set</Text>
                  </Button>
                  <Button
                    block
                    info
                    onPress={() => {
                      navigation.goBack();
                      tap.play();
                    }}>
                    <Text>Cancel</Text>
                  </Button>
                </View>
              </View>
            </View>
          )}
          {isSuccess && (
            <View authScreenWrapper>
              <H1 bold>Congratulations!</H1>
              <Text endDescription>
                You have created your new password. You can sign in using your new password.
              </Text>
              <View fullWidth center>
                <View half fullFormFooter>
                  <Button block primary onPress={() => navigation.navigate('Login')}>
                    <Text>Sign In</Text>
                  </Button>
                </View>
              </View>
            </View>
          )}
        </View>
        {isLoading && <ScreenOverlayLoading />}
      </DikeBackground>
    );
  }
}
