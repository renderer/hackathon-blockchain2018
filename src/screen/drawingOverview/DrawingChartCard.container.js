import { connect } from 'react-redux';
import DrawingChartCard from './DrawingChartCard.component';

import { getDrawingChart } from 'dike/store/drawing/drawing.actions';

export default connect(
  ({ drawing }) => ({
    drawingChart: drawing.drawingChart
  }),
  dispatch => ({
    onGetDrawingChart() {
      dispatch(getDrawingChart());
    }
  })
)(DrawingChartCard);
