import React from 'react';
import { Content } from 'native-base';

import DikeBackground from 'dike/components/DikeBackground';
import DrawingChartCard from './DrawingChartCard.container';

export default () => (
  <DikeBackground>
    <Content>
      <DrawingChartCard />
    </Content>
  </DikeBackground>
);
