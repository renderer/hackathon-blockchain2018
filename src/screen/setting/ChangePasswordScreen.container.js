import { connect } from 'react-redux';
import ChangePasswordScreen from './ChangePasswordScreen.component';

import { changePassword } from 'dike/store/user/user.actions';

export default connect(
  ({ app }) => ({ ...app.changePasswordScreen }),
  dispatch => ({
    onChangePassword({ oldPassword, newPassword }, onSuccess) {
      dispatch(changePassword({ oldPassword, newPassword }, onSuccess));
    }
  })
)(ChangePasswordScreen);
