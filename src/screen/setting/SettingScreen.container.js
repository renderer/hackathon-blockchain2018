import { connect } from 'react-redux';
import SettingScreen from './SettingScreen.component';

import { signOut } from 'dike/store/user/user.actions';

export default connect(
  null,
  dispatch => ({
    onSignOut() {
      dispatch(signOut());
    }
  })
)(SettingScreen);
