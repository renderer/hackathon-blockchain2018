import React from 'react';
import { tap } from 'dike/utils/sounds';
import BigNumber from 'bignumber.js';

import { View, Text, Button, H1 } from 'native-base';

import ScreenModal from 'dike/components/ScreenModal';
import BlinkingView from 'dike/components/BlinkingView';

const isValidValue = value => !!parseInt(value, 10) && !isNaN(parseInt(value, 10));

export default ({ value, multiply = 0, total = 0, onDismiss, onSubmit }) => (
  <ScreenModal
    onDismiss={() => onDismiss()}
    containerStyle={{ height: '60%' }}
    title="Stake"
    disabledTouchEnd>
    <View marginAround bgMain round>
      <View input>
        {isValidValue(value) && <H1 contrast>{new BigNumber(value).toFormat(0)}</H1>}
        <BlinkingView>
          <H1 contrast>|</H1>
        </BlinkingView>
      </View>
    </View>
    <Text right>x {multiply}</Text>
    <Text right>
      Total: {isValidValue(value) ? new BigNumber(value).times(multiply).toFormat(0) : 0}
    </Text>
    <View marginAround textAround>
      <Button
        success
        fixedWidth
        onPress={() => {
          onDismiss();
          tap.play();
        }}>
        <Text>Cancel</Text>
      </Button>
      <Button fixedWidth onPress={() => onSubmit()}>
        <Text>OK</Text>
      </Button>
    </View>
  </ScreenModal>
);
