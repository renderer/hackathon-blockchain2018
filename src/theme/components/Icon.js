import palette from '../palette';

export default variables => {
  return {
    '.highlight': { color: palette.text.highlight },
    '.larger': { fontSize: variables.fontSizeBase * 2.4 },
    '.small': { fontSize: variables.fontSizeBase },
    '.medium': { fontSize: variables.fontSizeH3 },
    '.success': { color: palette.dikeNumber.normal },
    color: palette.text.main
  };
};
