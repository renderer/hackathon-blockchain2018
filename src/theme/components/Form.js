export default variables => ({
  '.full': {
    width: '100%',
    'NativeBase.Item': {
      '.noLabel': {
        marginLeft: 0,
        marginTop: variables.spacingUnit * 4
      },
      '.inputLine': {
        height: 50
      },
      marginLeft: 0
    },
    marginBottom: variables.spacingUnit * 10
  }
});
