import React from 'react';

import { TouchableOpacity } from 'react-native';
import { Text } from 'native-base';

export default ({ text, onPress, TextProps = { highlight: true }, disabled }) => {
  const textProps = { ...TextProps, blur: disabled };
  return (
    <TouchableOpacity onPressIn={onPress}>
      <Text {...textProps}>{text}</Text>
    </TouchableOpacity>
  );
};
