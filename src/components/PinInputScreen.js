import theme from 'dike/theme';
import React, { Component } from 'react';

import { Dimensions, StyleSheet } from 'react-native';
import { Text, View, H1 } from 'native-base';

import BlinkingView from './BlinkingView';
import DikeBackground from './DikeBackground';
import Keyboard from './Keyboard';
import ScreenOverlayLoading from './ScreenOverlayLoading';
import TextButton from './TextButton';

import withDisableDelay from 'dike/utils/hoc/withDisableDelay';

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  mainScreen: { flex: 0.6, alignItems: 'center' },
  keyBoard: { flex: 0.4 },
  pinInput: { height: theme.spacingUnit * 8 }
});

const ResendButton = withDisableDelay(5000, disabled => ({ disabled }))(TextButton);

export default class PinInputScreen extends Component {
  static defaultProps = {
    title: 'Enter the code you received',
    errorNotice: 'Incorrect verification code',
    showError: false
  };

  constructor(props) {
    super(props);
    this.state = {
      code: []
    };
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput(value) {
    let code = this.state.code;
    const { onCodeVerify, onHideError, showError } = this.props;
    if (showError && onHideError) {
      onHideError();
    }
    if (value === 'BackSpace') {
      this.setState({ code: [] });
      return;
    }
    code.push(value.toString());
    if (code.length === 4) {
      const inputedCode = code.join('');
      onCodeVerify(inputedCode);
      code = [];
    }
    this.setState({ code });
  }
  render() {
    const { code } = this.state;
    const { title, description, onResend, isLoading, errorNotice, showError } = this.props;
    const blinkingIndex = this.state.code.length;

    return (
      <DikeBackground>
        <View style={styles.mainScreen}>
          <View full center authScreenWrapper>
            <H1 bold center>
              {title}
            </H1>
            <Text endDescription center>
              {description}
            </Text>
            <View horizontal half style={styles.pinInput}>
              {[0, 1, 2, 3].map(index => (
                <View key={index} center rowFlexItem borderBottom>
                  {!!code[index] && <H1 bold>{code[index]}</H1>}
                  {index === blinkingIndex && (
                    <BlinkingView>
                      <H1>|</H1>
                    </BlinkingView>
                  )}
                </View>
              ))}
            </View>
            {!!showError && <Text error>*{errorNotice}</Text>}
            <ResendButton
              TextProps={{ highlight: true }}
              text="Resend code"
              onPress={() => onResend()}
            />
          </View>
        </View>
        <View style={styles.keyBoard}>
          <Keyboard
            onInput={value => this.handleInput(value)}
            height={height * 0.4}
            hideModeBtn={true}
          />
        </View>
        {isLoading && <ScreenOverlayLoading />}
      </DikeBackground>
    );
  }
}
