import React from 'react';
import { Spinner, Card } from 'native-base';

export default ({ CardProps = { bgMain: true } }) => (
  <Card {...CardProps}>
    <Spinner />
  </Card>
);
