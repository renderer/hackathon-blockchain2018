import theme from 'dike/theme';
import { connect } from 'react-redux';
import React from 'react';

import { StyleSheet } from 'react-native';
import { View, Icon, Text } from 'native-base';

const styles = StyleSheet.create({
  wrapper: { marginLeft: theme.spacingUnit * 4 },
  text: { paddingLeft: theme.spacingUnit * 2 }
});

const DrawingCountdownHeader = ({ active }) => (
  <View horizontal center style={styles.wrapper}>
    <Icon
      name="ios-radio-button-on"
      small
      style={{ color: active ? theme.dikeNumber.normal : theme.dikeNumber.special }}
    />
    <Text style={styles.text}>
      {active ? 'Drawing in progress...' : 'Wait for the next drawing...'}
    </Text>
  </View>
);

export default connect(({ drawing }) => ({
  active: drawing.drawingStatus.inProgress
}))(DrawingCountdownHeader);
