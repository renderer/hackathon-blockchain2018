import theme from 'dike/theme';
import React from 'react';

import { StyleSheet } from 'react-native';
import { View, Text } from 'native-base';

import { padZero } from 'dike/utils/common/numbers';

const styles = StyleSheet.create({
  occurence: {
    position: 'absolute',
    bottom: 0,
    right: 0
  }
});

const NumberWithOccurence = ({ children, size, occurence }) => (
  <View
    center
    style={{
      position: 'relative',
      width: size + theme.spacingUnit,
      height: size + theme.spacingUnit
    }}>
    {children}
    {!!occurence &&
      occurence > 1 && (
        <View
          center
          horizontal
          bgDikeSpecial
          style={[
            styles.occurence,
            {
              width: size / 2.5,
              height: size / 2.5,
              borderRadius: size / 5
            }
          ]}>
          <Text style={{ fontSize: size / 5 }}>x</Text>
          <Text bold style={{ fontSize: size / 4 }}>
            {occurence}
          </Text>
        </View>
      )}
  </View>
);

const DikeNumber = ({ number, special, size, lost }) => (
  <View
    center
    bgBlur={lost}
    bgDikeSpecial={!lost && special}
    bgDikeNormal={!lost && !special}
    style={{ borderRadius: size / 2, width: size, height: size }}>
    <Text bold primary={lost} style={{ fontSize: size / 2.5 }}>
      {padZero(number)}
    </Text>
    {special && (
      <Text primary={lost} style={{ fontSize: size / 6 }}>
        Special
      </Text>
    )}
  </View>
);

export default ({ number, special = false, size = 50, occurence, lost = false }) => {
  if (occurence != null && !lost) {
    return (
      <NumberWithOccurence size={size} occurence={occurence}>
        <DikeNumber number={number} special={special} size={size} />
      </NumberWithOccurence>
    );
  }
  return <DikeNumber number={number} special={special} size={size} lost={lost} />;
};
