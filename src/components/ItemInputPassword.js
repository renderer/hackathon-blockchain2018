import theme from 'dike/theme';
import React, { Component } from 'react';
import { Item, Input, Icon, Text } from 'native-base';

export default class ItemInputPassword extends Component {
  static defaultProps = {
    placeholder: 'Password',
    errorNotice: "Password's too short",
    error: false
  };

  constructor(props) {
    super(props);
    this.state = { isPasswordVisible: false };
  }

  handleIconPress() {
    const { value } = this.props;
    const { isPasswordVisible } = this.state;
    if (!value) {
      return;
    }

    this.setState({ isPasswordVisible: !isPasswordVisible });
  }

  render() {
    const { isPasswordVisible } = this.state;
    const { value, onChangeText, error, placeholder, errorNotice } = this.props;
    return (
      <React.Fragment>
        <Item error={error} noLabel>
          <Input
            ref={node => (this.input = node)}
            placeholderTextColor={theme.text.placeholder}
            placeholder={placeholder}
            autoCapitalize="none"
            value={value}
            onChangeText={v => onChangeText(v)}
            secureTextEntry={!isPasswordVisible}
          />
          <Icon
            name={isPasswordVisible ? 'ios-eye-off' : 'ios-eye'}
            style={{ color: theme.text.placeholder }}
            onPress={() => this.handleIconPress()}
          />
        </Item>
        {error && (
          <Text error small>
            *{errorNotice}
          </Text>
        )}
      </React.Fragment>
    );
  }
}
