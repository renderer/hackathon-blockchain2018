import theme from 'dike/theme';
import React from 'react';
import { StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const styles = StyleSheet.create({
  dikeBg: { flex: 1, position: 'relative' }
});

export default ({ children }) => (
  <LinearGradient colors={[theme.bgGradient.start, theme.bgGradient.end]} style={styles.dikeBg}>
    {children}
  </LinearGradient>
);
