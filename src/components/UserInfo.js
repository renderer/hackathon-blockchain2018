import { connect } from 'react-redux';
import React from 'react';

import { View, Text, Thumbnail, H2, H1 } from 'native-base';

const UserInfo = ({
  username,
  email,
  profilePicture,
  balance,
  showBalance = false,
  ViewProps = {},
  displayBadges,
  lpPoints
}) => (
  <View center {...ViewProps}>
    {profilePicture && <Thumbnail large source={{ uri: profilePicture }} />}
    <H2>{username}</H2>
    <Text>{email}</Text>
    <H1 bold>{lpPoints ? lpPoints : 0} LP</H1>
    <View style={{ flexDirection: 'row', marginTop: 5 }}>
      {displayBadges ? displayBadges.map((badge, i) => <Text key={i}>{badge}</Text>) : ''}
    </View>
  </View>
);

export default connect(({ user }) => ({
  username: user.username,
  profilePicture: user.profilePicture,
  email: user.email,
  balance: user.balance,
  lpPoints: 1000,
  displayBadges: ['️🥇', '️🏆', '️🏅']
}))(UserInfo);
